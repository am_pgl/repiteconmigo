package com.cip.pdm.repiteconmigo;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

public class RepiteConmigoActivity extends AppCompatActivity {

    private EditText edit_repetir;
    private TextView text_repetir;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repite_conmigo);

        edit_repetir=(EditText)findViewById(R.id.edittext_rep);
        text_repetir=(TextView)findViewById(R.id.textView_rep);

        edit_repetir.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                text_repetir.setText(edit_repetir.getText().toString());
            }
        });

    }
}
